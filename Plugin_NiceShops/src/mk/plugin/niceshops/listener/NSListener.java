package mk.plugin.niceshops.listener;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerJoinEvent;

import mk.plugin.niceshop.offlinegive.OfflineGives;
import mk.plugin.niceshops.gui.GUIShop;
import mk.plugin.niceshops.gui.GUIView;
import mk.plugin.niceshops.main.MainNiceShops;

public class NSListener implements Listener {
	
	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		Bukkit.getScheduler().runTaskLater(MainNiceShops.get(), () -> {
			OfflineGives.checkOfflineGive(MainNiceShops.get(), e.getPlayer());
		}, 20);
	}
	
	@EventHandler
	public void onClick(InventoryClickEvent e) {
		GUIShop.onClick(e);
		GUIView.onClick(e);
	}
	
	@EventHandler
	public void onClose(InventoryCloseEvent e) {
		GUIView.onClose(e);
	}
	
}
