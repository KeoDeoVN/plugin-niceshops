package mk.plugin.niceshops.main;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import mk.plugin.niceshop.offlinegive.OfflineGives;
import mk.plugin.niceshops.command.AdminCommand;
import mk.plugin.niceshops.config.Configs;
import mk.plugin.niceshops.listener.NSListener;
import mk.plugin.niceshops.price.PriceType;
import mk.plugin.niceshops.storage.ItemStorage;
import mk.plugin.niceshops.storage.ShopStorage;
import mk.plugin.niceshops.yaml.YamlFile;

public class MainNiceShops extends JavaPlugin {
	
	private static MainNiceShops inst;
	
	@Override
	public void onEnable() {
		this.loadInstance();
		this.reloadConfigs();
		this.registerListeners();
		this.registerCommands();
	}
	
	@Override
	public void onDisable() {
		Bukkit.getOnlinePlayers().forEach(player -> player.closeInventory());
	}
	
	public void loadInstance() {
		inst = this;
	}
	
	public void reloadConfigs() {
		this.saveDefaultConfig();
		YamlFile.reloadAll(this);
		Configs.load(YamlFile.CONFIG.get());
		PriceType.loadAll(YamlFile.CONFIG.get());
		ItemStorage.reload(this);
		ShopStorage.reload(this);
		OfflineGives.reload(this);
	}
	
	public void registerListeners() {
		Bukkit.getPluginManager().registerEvents(new NSListener(), this);
	}
	
	public void registerCommands() {
		this.getCommand("niceshops").setExecutor(new AdminCommand());
	}
	
	public static MainNiceShops get() {
		return inst;
	}
	
}
